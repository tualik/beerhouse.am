<a href="/">
    <button type="button" class="btn btn-menu">Գլխավոր</button>
</a>

<a href="/about">
    <button type="button" class="btn btn-menu">Մեր Մասին</button>
</a>

<a href="/news">
    <button type="button" class="btn btn-menu">Նորություններ</button>
</a>

<a href="/delivery">
    <button type="button" class="btn btn-menu">Առաքում</button>
</a>

<a href="/cart">
    <button type="button" class="btn btn-menu">Զամբյուղ</button>
</a>

<a href="/shops">
    <button type="button" class="btn btn-menu">Խանութներ</button>
</a>

<a href="/partners">
    <button type="button" class="btn btn-menu">Գործընկերներ</button>
</a>

<a href="/feedback">
    <button type="button" class="btn btn-menu">Կարծիքներ</button>
</a>

<a href="/vacancy">
    <button type="button" class="btn btn-menu">Աշխատանք</button>
</a>

<a href="/contacts">
    <button type="button" class="btn btn-menu">Կապ</button>
</a>