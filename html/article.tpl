<div class="panel panel-danger">
    <div class="panel-body" style="">
        <?php
$url_array = Category::get_all_url();
?>
        <div class="row">
            <form method="post" action="" class="jcart">
                <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>"/>
                <input type="hidden" name="my-item-id" value="<?=$registry->article_data[0]['idarticle']?>"/>
                <input type="hidden" name="my-item-name" value="<?=$registry->article_data[0]['title']?>"/>
                <input type="hidden" name="my-item-price" value="<?=$registry->article_data[0]['price']?>"/>
                <input type="hidden" name="my-item-url" value="<?=$registry->url[0]?>"/>

                <div class="col-xs-5">
                    <a href="<?=$registry->article_data[0]['full_image_url']?>" data-lightbox="image-article"
                       title="<?=$registry->article_data[0]['image_alt']?>">
                        <img class="img-responsive img-rounded" src="<?=$registry->article_data[0]['full_image_url']?>"
                             alt="<?=$registry->article_data[0]['image_alt']?>">
                    </a>
                </div>
                <div class="col-xs-4">
                    <h4 class="product-name"><strong class="text-primary"><a
                                    href="<?php Category::get_url($registry->article_data[0]['idurl'], $url_array); ?>"><?=$registry->
                                article_data[0]['title']?></a></strong>
                    </h4>
                    <h4>
                        <small><?=$registry->article_data[0]['description']?> <?=$registry->article_data[0]['text']?>
                        </small>
                    </h4>
                    <h5><strong class="text-danger" style="font-size: 16px;"> <?=$registry->article_data[0]['price']?>
                            դր</strong></h5>
                </div>
                <div class="col-xs-3">

                    <div class="col-xs-1">
                        Քանակը:
                    </div>
                    <br>

                    <div class="col-xs-2">
                        <span class="glyphicon glyphicon-minus-sign minus text-primary"</span>
                        <span><input type="text" class="" name="my-item-qty" value="1" size="1"></span>
                        <span class="glyphicon glyphicon-plus-sign text-danger plus"</span>
                    </div>

                    <div>
                        <input style="

                         background: #002919; /* Old browsers */
    background: -moz-linear-gradient(top, #002919 0%, #002919 25%, #025535 51%, #012900 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#002919), color-stop(25%,#002919), color-stop(51%,#025535), color-stop(100%,#012900)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* IE10+ */
    background: linear-gradient(to bottom, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002919', endColorstr='#012900',GradientType=0 ); /* IE6-9 */

                        margin-top: 10px;" type="submit" name="my-add-button" value="Պատվիրել"
                               class="button btn btn-info"/>
                    </div>
                </div>
            </form>
        </div>
        <hr>

    </div>
    <?php //var_dump($registry->article_data);?>
</div>