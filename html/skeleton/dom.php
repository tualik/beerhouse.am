<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:11 PM
 * Project: testing.beerhouse.am
 * File: dom.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="<?= $this->registry->dom_language ?>">
<head>
    <?php
    require_once SKELETON_DIR . "/head.php";
    ?>
    <style>
        body {
            background-image: url(/html/images/back.jpg); /* Путь к фоновому изображению */
            background-color: #c7b39b; /* Цвет фона */
        }
    </style>
</head>
<body>
<div itemscope itemtype="http://schema.org/Restaurant" id="wrap">
<div class="container" style="width: 1200px;">
        <?php
        require_once SKELETON_DIR . "/body_header.php";
        require_once SKELETON_DIR . "/body_content.php";
        ?>
    </div>
</div>
<?php
require_once SKELETON_DIR . "/body_footer.php";
////////////////////////////////////////////////////////////////////////////////////////
?>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/html/skeleton/js/bootstrap.min.js"></script>
<script src="/html/skeleton/js/application.js"></script>
<script src="/html/skeleton/js/plusminus.js"></script>
<script src="/html/skeleton/js/lightbox-2.6.min.js"></script>
<script src="/html/skeleton/js/jcart.min.js"></script>
</body>
</html>