<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:11 PM
 * Project: testing.beerhouse.am
 * File: body_header.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
?>
<br>
<div id="body_header" style="max-width: 1200px;">
    <div class="panel " style=" background: #002919; /* Old browsers */
    background: -moz-linear-gradient(top, #002919 0%, #002919 25%, #025535 51%, #012900 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#002919), color-stop(25%,#002919), color-stop(51%,#025535), color-stop(100%,#012900)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* IE10+ */
    background: linear-gradient(to bottom, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002919', endColorstr='#012900',GradientType=0 ); /* IE6-9 */">
        <div class="row">
            <div class="col-md-4">
                <br>
                <?php Render::widget('logo'); ?>
            </div>
            <div class="col-md-8">
                <br>
                <?php Render::widget('beer_logo'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php Render::widget('carousel'); ?>
        </div>
    </div>

</div>