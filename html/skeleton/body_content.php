<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:12 PM
 * Project: testing.beerhouse.am
 * File: body_content.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
?>
<br>
<div style="width: 1170px;">
    <div class="row">
        <div class="col-mg-12 text-center" >
            <?php self::show("top_menu"); ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-3" style=""><?php /// LEFT SIDEBAR ?>
            <?php self::show("left_sidebar", $this->registry, $this->debug); ?>
        </div>
        <div class="col-md-6">
            <?php /// CONTENT
            self::show($this->registry->content_template, $this->registry, $this->debug);
            ?>
        </div>
        <div class="col-md-3"><?php /// RIGHT SIDEBAR ?>
            <?php self::show("right_sidebar", $this->registry, $this->debug); ?>
        </div>
    </div>
</div>