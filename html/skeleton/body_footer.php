<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:12 PM
 * Project: testing.beerhouse.am
 * File: body_footer.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
?>
<br>
<meta itemprop="email" content="info@beerhouse.am"> <span itemprop="openingHoursSpecification" itemscope
                                                          itemtype="http://schema.org/OpeningHoursSpecification"> <span
        itemprop="dayOfWeek" itemscope itemtype="http://schema.org/DayOfWeek"> <meta itemprop="name" content="7"></span> <meta
        itemprop="opens"
        content="Укажите допустимую дату и время по стандарту ISO 8601. Например: 2015-07-27 или 2015-07-27T15:30"> <meta
        itemprop="closes"
        content="Укажите допустимую дату и время по стандарту ISO 8601. Например: 2015-07-27 или 2015-07-27T15:30"></span>
<meta itemprop="servesCuisine" content="Armenian, German">
<meta itemprop="name" content="Krombacher www.BeerHouse.am"></div>
<div class="container text-center">
    <div class="panel panel-warning" style=" background: #002919; /* Old browsers */
    background: -moz-linear-gradient(top, #002919 0%, #002919 25%, #025535 51%, #012900 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#002919), color-stop(25%,#002919), color-stop(51%,#025535), color-stop(100%,#012900)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* IE10+ */
    background: linear-gradient(to bottom, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002919', endColorstr='#012900',GradientType=0 ); /* IE6-9 */">
        <br>

        <p>| <a href="/">Գլխավոր</a> | <a href="/cart">Զամբյուղ</a> | <a
                href="/news">Նորություններ</a> | <a
                href="/partners">Գործընկերներ</a> |
            <a href="/locations">Խանութներ</a> | <a href="/delivery">Առաքում</a> | <a href="/vacancy">Աշխատանք</a>
            | <a href="/feedback">Կարծիքներ</a> |
            <a href="/about">Մեր Մասին</a> | <a href="/contacts">Հասցեներ</a> | </p>
        <hr>
        <p class="text-warning">Copyright 2014 &copy <a href="http://wancloud.org"> Wancloud LLC</a>.</p>
    </div>
</div>