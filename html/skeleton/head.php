<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:11 PM
 * Project: testing.beerhouse.am
 * File: head.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?= $this->registry->dom_title ?></title>
    <a href="https://plus.google.com/108236484963991257845? rel=author">Google</a>
    <meta name="google-site-verification" content="DzDgy9AqG0VENmpYmflus8Lv9tiHHJErmsLN8nuEDhE" />
    <!-- Bootstrap -->
    <link href="/html/skeleton/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/html/skeleton/css/sticky_footer.css" rel="stylesheet">
    <link href="/html/skeleton/css/carousel.css" rel="stylesheet">
    <link href="/html/skeleton/css/lightbox.css" rel="stylesheet"/>
    <link href="/html/skeleton/css/jcart.css" rel="stylesheet"/>
    <!--link rel="stylesheet" type="text/css" media="screen, projection" href="html/skeleton/css/jcart.css" />
    <!--link href="/html/skeleton/css/cart.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
    <script>
        var _prum = [['id', '53a5a33cabe53dd64a6efa18'],
            ['mark', 'firstbyte', (new Date()).getTime()]];
        (function() {
            var s = document.getElementsByTagName('script')[0]
                , p = document.createElement('script');
            p.async = 'async';
            p.src = '//rum-static.pingdom.net/prum.min.js';
            s.parentNode.insertBefore(p, s);
        })();
    </script>
<?php

if ($this->registry->attaching != "") {
    // var_dump($this->registry->attaching);
    require_once HTML_DIR . "/" . $this->registry->attaching . ".tpl";

}
?>