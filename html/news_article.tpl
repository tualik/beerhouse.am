<div class="panel panel-danger">
    <div class="panel-body" style="background: rgb(252,227,196); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZTNjNCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZmM1NzgiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(252,227,196,1) 0%, rgba(255,197,120,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(252,227,196,1)), color-stop(100%,rgba(255,197,120,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(252,227,196,1) 0%,rgba(255,197,120,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(252,227,196,1) 0%,rgba(255,197,120,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(252,227,196,1) 0%,rgba(255,197,120,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(252,227,196,1) 0%,rgba(255,197,120,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fce3c4', endColorstr='#ffc578',GradientType=0 ); /* IE6-8 */">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="alert-message alert-message-warning">
                    <h4><?= $registry->news_article_data[0]["title"] ?></h4>

                    <p><?= $registry->news_article_data[0]["text"] ?></p>
                    <span class="label label-primary"><?= substr($registry->
                        news_article_data[0]["date"], 0 ,10) ?></span>
                </div>
            </div>
        </div>


    </div>
    <?php
//var_dump($registry->news_article_data);
    ?>
</div>