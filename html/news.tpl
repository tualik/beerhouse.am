<div class="panel panel-danger">
    <div class="panel-body" style="">

        <div class="row">
            <h3 class="panel-heading">Նորություններ</h3>

            <div class="col-sm-12 col-md-12">
                <?php
             $count = count($registry->news_data);
                for($n=0;$n<$count;$n++){
                ?>

                <div class="alert-message alert-message-danger">
                    <h4><a href="<?php echo Config::$CONFIG["HOST"] . "news/article/" .
                        $registry->news_data[$n]["idnews"] ?>"><?= $registry->news_data[$n]["title"]; ?></a></h4>

                    <p><?= $registry->news_data[$n]["description"] ?></p>
                    <span class="label label-info"><?= substr($registry->news_data[$n]["date"], 0 ,10) ?></span>
                </div>

                <?php } ?>
            </div>
        </div>

    </div>
    <?php
//var_dump($registry->news_data);
    ?>
</div>