<div class="panel panel-danger">
    <div class="panel-body" style="">

        <div class="row">
            <h3 class="panel-heading">Կարծիքներ</h3>

            <div class="col-sm-12 contact-form ">
                <form id="contact" method="post" class="form" role="form">
                    <div class="row">
                        <div class="col-xs-6 col-md-6 form-group">
                            <input class="form-control" id="name" name="name" placeholder="Անուն՝*" type="text"/>
                        </div>
                        <div class="col-xs-6 col-md-6 form-group">
                            <input class="form-control" id="email" name="email" placeholder="Էլ.հասցե`*" type="email"/>
                        </div>
                    </div>
                    <textarea class="form-control" id="message" name="message" placeholder="Հաղորդագրություն`*"
                              rows="5"></textarea>
                    <br/>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 form-group">
                            <button style="

                         background: #002919; /* Old browsers */
    background: -moz-linear-gradient(top, #002919 0%, #002919 25%, #025535 51%, #012900 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#002919), color-stop(25%,#002919), color-stop(51%,#025535), color-stop(100%,#012900)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* IE10+ */
    background: linear-gradient(to bottom, #002919 0%,#002919 25%,#025535 51%,#012900 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#002919', endColorstr='#012900',GradientType=0 ); /* IE6-9 */

                        margin-top: 10px;" class="btn btn-primary pull-right" name="send_feedback" value="sended" type="submit">Ուղարկել</button>
                            <!--Ուղարկել-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>