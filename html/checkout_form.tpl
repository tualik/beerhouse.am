<!-- sample modal content -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><a href="/">www.BeerHouse.am</a></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <h4>Արագ Պատվեր Միայն Հեռախոսահամարով</h4>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Անուն</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control input-sm"
                                           placeholder="Ձեր Անունը ․․․ " tabindex="2">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Էլ․ Հասցե</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control input-sm"
                                           placeholder="Էլեկտրոնային Հասցեն ․․․ " tabindex="2">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Հեռախոս*</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="phone_number" class="form-control input-sm"
                                           placeholder="Հեռախոսահամար ․․․ " tabindex="2" value="+374">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h4 class="text-danger">Հասցե</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Առաքման Հասցե</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="city_street" class="form-control input-sm"
                                           placeholder="Քաղաք, փողոց ․․․ " tabindex="2">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Բնակարան, տուն</h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="flat_number" class="form-control input-sm"
                                           placeholder="Բնակարան ․․․ " tabindex="2">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="home_number" class="form-control input-sm"
                                           placeholder="Տուն ․․․ " tabindex="2">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Լրացուցիչ Տվյալներ</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea name="more_details" class="form-control input-sm" rows="2" maxlength="255"
                                              cols="32" placeholder="Լրացուցիչ Տվյալներ ․․․ " tabindex="2"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h4 class="text-danger">Պատվերի Առաքում</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <br>
                                    <h5 class="text-primary">Երբ Առաքե՞լ</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <br>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#right_now" data-toggle="tab">Հնարավորինս արագ</a></li>
                                    <li><a href="#time" data-toggle="tab">Նախնական Պատվեր</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="right_now"></div>
                                    <div class="tab-pane fade" id="time">
                                        <br>
                                        <?= DateShow::show_date(null) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group text-right">
                                    <h5 class="text-primary">Ձեր Նշումները</h5>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="form-group">
                                        <textarea name="notes" class="form-control input-sm" rows="2" maxlength="255"
                                                  cols="32" placeholder="Ձեր Նշումները ․․․ " tabindex="2"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="colorgraph">
                        <?php
                                // PayPal checkout button
                                if ($config['button']['checkout']) {
                                    $inputType = "image";
                                    $src = " src='{$config['button']['checkout']}' alt='{$config['text']['checkoutPaypal']}' title='' class='btn btn-primary'";
                                }
                                if ($this->itemCount <= 0) {
                        $disablePaypalCheckout = " disabled='disabled'";
                        }
                        echo "<input type='$inputType' $src id='jcart-paypal-checkout' class='btn btn-primary'
                        name='jcartPaypalCheckout' value='{$config['text']['checkoutPaypal']}' $disablePaypalCheckout
                        />\n";
                        echo "<input type='$inputType' $src id='jcart-paypal-checkout' class='btn btn-primary'
                        name='jcartCheckoutCache' value='{$config['text']['checkoutCache']}' $disablePaypalCheckout
                        />\n";
                        ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h4 class="text-left" style="padding-left: 5%;">
                    <small> * ֊ պարտադիր դաշտեր</small>
                </h4>
                <?php
                    //include HTML_DIR . "/events/form_error.php";
                    //var_dump($_POST);
                    ?>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
<br>
<div class="text-left" style="padding-left: 2.5%">
    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" name="modal" type="button">
        Պատվիրել
    </button>

</div><!-- /example -->
