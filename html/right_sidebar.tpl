<!-- Cart -->
<?php if($registry->url[0] != "cart"){ ?>
<div class="panel" style="border-radius: 6px; background-color: #022f22">

    <div class="panel-body" style="">
        <div id="sidebar">
            <?php
            //var_dump($registry->url);
            if($registry->url[0] !== "checkout"){ ?>
            <div id="jcart"><?php $registry->cart->display_cart();?></div>
            <?php } else { ?>
            <div id="jcart"><?php $registry->cart->show_cart();?></div>
            <?php } ?>
        </div>
        <br>
        <a href="https://www.facebook.com/KrombacherBeerHouse.am" target="_blank"><img src="http://beerhouse.am/slidepics/facebooklike.jpg" alt="" style="width: 100%;" >
        </a>
    </div>
</div>
<?php } ?>
<!-- /Cart -->
<!-- Info Block -->
<div class="panel" style="background-color: #022f22;">
    <div class="panel-body text-center" style="color: #ffffff">
        <h1><b>ԱՌԱՔՈՒՄ</b></h1>

        <h1><b itemprop="telephone">010276071</b></h1>
        <h4 itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><b> <span itemprop="streetAddress">ԿԻԵՎՅԱՆ 1</span>,</b><b>
                11:30-23:30</b>
            <meta itemprop="addressLocality" content="Yerevan">
            <meta itemprop="addressCountry" content="Armenia">
            <meta itemprop="postalCode" content="0028">
        </h4>
        <h5>ԶԱՆԳՈՒ Ռեստորանային Համալիր</h5>
    </div>
</div>
<!-- /Info Block -->
<!-- News Block -->
<div class="panel" style="height: 100%; background-color: #022f22;">
    <div class="panel-heading">
        <h3 class="panel-title">Նորություններ</h3>
    </div>
    <div class="panel-body" style="background-color: #022f22;">
        <?php
        $news = NewsWidget::get_news_widget();

        $count = count($news);
        if ($count > 10) {
        $count = 10;
        }
        for ($n = 0; $n < $count; $n++) {
        ?>
        <div class="panel-warning">
            <p><strong ><a style="color: #ffffff;"href="<?= Config::$CONFIG[" HOST"] . "news/article/" .$news[$n]["idnews"]
                    ?>"><?= $news[$n]["title"] ?></a></strong></p>

            <p><?= $news[$n]["description"] ?></p>
            <span class="label label-warning"><?= substr($news[$n]["date"], 0 ,10) ?></span>
            <hr>
        </div>
        <?php } ?>
        <a href="/news">Բոլորը․․․․</a>
    </div>
</div>
<!-- /News Block -->