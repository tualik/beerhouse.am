<?php

/**
 * Created by 3M-LiFe Group Co., Ltd.
 * Owner: Aram Harutyunyan
 * Date: 11/4/13
 * Time: 2:22 AM
 * Project: hmvc.wancloud.org
 * File: database.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
class Database
{
    public $data = array();
    public $request;
    private $db;
    private $result;
    private $database;
    private $column = array();
    private $values = array();
    private $charset;

    //private $Timer;

    public function __construct($data, $request, $port = null, $charset = 'utf8')
    {
        echo '<h6>"Database Class" loading...</h6>';
        $this->Timer = new MicroTimer();
        $this->data = $data; //data[0-6] control options data[7-...] column values
        $this->request = $request; // $request[0] - function type{connect,...}; $request[1] - function of request{insert,update,select ...};
        $this->database = Config::$CONFIG["DB_NAME"];
        $this->charset = $charset;
        //$this->data_parser($data);
        $this->request_trigger();

    }

    public function connect_mysql()
    {
        if ($this->request[0] == "connect") {
            $this->db = new mysqli(Config::$CONFIG["DB_HOST"], Config::$CONFIG["DB_USER"], Config::$CONFIG["DB_PASS"], Config::$CONFIG["DB_NAME"], $this->port);
            $this->db->set_charset($this->charset);
            /* проверка подключения */
            if (mysqli_connect_errno()) {
                printf("Connection Error: %s\n", mysqli_connect_error());
                echo "<br>";
                exit();
            } else {
                printf("Connection Successful: %s\n", mysqli_connect_error());
                echo "<br>";
            }
            return true;
        } else {
            return false;
        }
    }

    public function close_connection()
    {
        if ($this->db->close()) {
            printf("Connection Closed: %s\n", mysqli_connect_error());
            echo "<br>";
        }
        return true;
    }

    public function get_table_columns($table)
    {

        //////////////////////////////////////////////////////
        if (function_exists('objectToArray')) {
        } else {
            function objectToArray($object)
            {
                if (!is_object($object) && !is_array($object)) {
                    return $object;
                }
                if (is_object($object)) {
                    $object = get_object_vars($object);
                }
                return array_map('objectToArray', $object);
            }
        }
        //////////////////
        //////////////////////////////////////////////////////
        $query = "SELECT * FROM " . $table . "";
        if ($result = $this->db->query($query)) {
            $columns = $result->fetch_fields();
            $array = objectToArray($columns);
            for ($j = 0; $j < count($array); $j++) {
                $this->column[$j] = $array[$j]["name"];
            }

            /*
               name 	  Имя столбца
               orgname 	  Исходное имя столбца, если у него есть псевдоним
               table 	  Имя таблицы, которой принадлежит столбец (если не вычислено)
               orgtable   Исходное имя таблицы, если есть псевдоним
               max_length Максимальная ширина поля результирующего набора.
               length 	  Ширина поля, как она задана при определении таблицы.
               charsetnr  Количество наборов символов для поля.
               flags 	  Целое число, представляющее битовые флаги для поля.
               type 	  Тип данных поля
               decimals   Число знаков после запятой (для целочисленных полей)
            */
            $result->close();
        } else {
            echo "errors in get_table_columns function,- now script die";
            //die;
        }
        //$this->close_connection();
        return true;
    }

    public function get_table_values($data)
    {
        ///////////////////////////////////////////////////////
        ///data[0-6] control options data[7-...] column values
        ///////////////////////////////////////////////////////
        for ($i = 7; $i < $data[6]; $i++) {
            $this->values[$i] = $data[$i];
        }
        //var_dump($this->values);
    }

    public function data_parser()
    {
        if ($this->request[0] == "connect") {
            if ($this->request[1] == "insert" || $this->request[1] == "update") {
                $this->get_table_columns($this->data["table"]);
                $this->get_table_values($this->data);
            } else {
                // echo "going to '" . $this->request[1] . "' function, data_parser passed without enter <br>";
            }
        } else { /////// else ....
            echo "errors in data_parser function,- now script die <br>";
            die;
        }
    }

    public function request_trigger()
    {
        //$this->request[0]="connect";
        //$this->request[1]="select";
        if (!is_object($this->db)) {
            if ($this->request[0] == "connect") {
                $this->connect_mysql();
                $this->data_parser($this->data);
                if ($this->request[1] == "insert") {
                    $this->insert($this->data["table"], $this->column, $this->values);
                }
                if ($this->request[1] == "update") {
                    $this->update($this->data["table"], $this->column, $this->values, $this->data["identify"], $this->data["key"]);
                }
                if ($this->request[1] == "delete") {
                    $this->delete($this->data["table"], $this->data["identify"], $this->data["key"]);
                }
                if ($this->request[1] == "delete_many") {
                    $this->delete_many($this->data["table"], $this->data["identify"], $this->data["key"], $this->data["identify_2"], $this->data["key_2"]);
                }
                if ($this->request[1] == "select_by") {
                    $this->select_by($this->data["table"], $this->data["identify"], $this->data["key"]);
                }
                if ($this->request[1] == "select") {
                    $this->select($this->data["table"]);
                }
                if ($this->request[1] == "select_result") {
                    $this->select_result($this->data["table"]);
                }
                if ($this->request[1] == "select_result_order_by") {
                    $this->select_result_order_by($this->data["table"], $this->data["order_by"]);
                }
                if ($this->request[1] == "select_where") {
                    $this->select_where($this->data["column"], $this->data["table"], $this->data["identify"], $this->data["key"]);
                }
                if ($this->request[1] == "select_where_many") {
                    $this->select_where_many($this->data["column"], $this->data["table"], $this->data["identify"], $this->data["key"], $this->data["identify_2"], $this->data["key_2"]);
                }
                if ($this->request[1] == "select_all_where_many_fixed") {
                    $this->select_all_where_many_fixed($this->data["table"], $this->data["identify"], $this->data["key"], $this->data["identify_2"], $this->data["key_2"]);
                }
                if ($this->request[1] == "select_all_where") {
                    $this->select_all_where($this->data["table"], $this->data["identify"], $this->data["key"]);
                }
                if ($this->request[1] == "select_all_where_many") {
                    $this->select_all_where_many($this->data["table"], $this->data["identify"], $this->data["key"] /*key = array, 1215,155,15,,*/);
                }
                if ($this->request[1] == "query_result") {
                    $this->query_result($this->data["query"]);
                }
                if ($this->request[1] == "query") {
                    $this->query($this->data["query"]);
                }
                $this->close_connection();
            } // $request[0] - function type{connect,...}; $request[1] - function of request{insert,update,select ...};
        } else { ////// else ....
            echo "errors in request_trigger function,- now script die <br>";
            die;
        }
        return $this->result;
    }

    public function query($query)
    {
        if (!$this->db)
            return false;
        if (is_object($this->result))
            $this->result->free();
        $this->result = $this->db->query($query);
        if ($this->db->errno) {
            $alert = '--> MySQLi ERRoR   " #' . $this->db->errno . ': ' . $this->db->error . ' <-- <br>';
            require_once HTML_DIR . '/events/alert.tpl';
            die('<b>--> MySQLi ERRoR   " #' . $this->db->errno . ': ' . $this->db->error . ' <--</b> <br>');
        }
        if (is_object($this->result)) {
            while ($row = $this->result->fetch_assoc())
                $data[] = $row;
            return $data;
        } else if ($this->result == FALSE)
            return false;
        else return $this->db->affected_rows;
    }

    public function query_result($query)
    {
        if (!$this->db)
            return false;
        if (is_object($this->result))
            $this->result->free();
        echo $query;
        $this->result = $this->db->query($query);
        if ($this->db->errno) {
            die('--> MySQLi ERRoR   " #' . $this->db->errno . ': ' . $this->db->error);
        }
        return $this->result;

    }

    public function insert($table, $column, $values)
    {
        $tr = implode("`, `", $column);
        $trv = implode("', '", $values);
        $query_insert = "INSERT INTO `" . $this->database . "`.`" . $table . "` (`" . $tr . "`) VALUES ('" . $trv . "');"; // DB REQUEST
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //$Db = new Db($this->host, $this->user, $this->password, $this->database); // connection ,initialization
        echo "Database -->>" . $query_insert . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_insert"); // query
        return $this->result; // making result
    }

    public function update($table, $columns, $values, $identify, $key)
    {
        $r_table = "";
        if (count($columns) == count($values)) {
            for ($i = 0; $i < count($columns); $i++) {
                if ($i >= 1) {
                    $p = ", ";
                } else {
                    $p = "";
                }
                $r_table .= " " . $p . "`" . $columns[$i] . "` = '" . $values[$i] . "' ";
            }
        } else {
            echo "FUCKED VALUES";
        }
        $query_update = "UPDATE `" . $this->database . "`.`" . $table . "` SET " . $r_table . " WHERE `" . $table . "`.`" . $identify . "` = '" . $key . "';"; // DB REQUEST
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        echo "Database -->>" . $query_update . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_update"); // query
        return $this->result; // making result
    }

    public function delete($table, $identify, $key)
    {
        $query_delete = "DELETE FROM `" . $this->database . "`.`" . $table . "` WHERE `" . $table . "`.`" . $identify . "` = '" . $key . "';"; // DB REQUEST
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_delete . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_delete"); // query
        return $this->result; // making result
    }

    public function delete_many($table, $identify, $key, $identify_2, $key_2)
    {
        $query_delete = "DELETE FROM `" . $this->database . "`.`" . $table . "` WHERE `" . $identify . "` = '" . $key . "' AND `" . $identify_2 . "` = '" . $key_2 . "';"; // DB REQUEST
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_delete . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_delete"); // query
        return $this->result; // making result
    }


    public function select_by($table, $identify, $key)
    {
        $query_select_by = "SELECT * FROM `" . $this->database . "`.`" . $table . "` WHERE `" . $identify . "` = '" . $key . "';"; // DB REQUEST
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select_by . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select_by"); // query
        return $this->result; // making result
    }

    public function select($table)
    {
        $query_select = "SELECT * FROM `" . $this->database . "`.`" . $table . "`;"; // DB REQUEST
        /////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select"); // query
        return $this->result; // making result
    }

    public function select_result($table)
    {
        $query_select = "SELECT * FROM `" . $this->database . "`.`" . $table . "`;"; // DB REQUEST
        /////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query_result("$query_select"); // query
        return $this->result; // making result
    }

    public function select_result_order_by($table, $order_by)
    {
        $query_select = "SELECT * FROM `" . $this->database . "`.`" . $table . "` ORDER BY `" . $order_by . "`;"; // DB REQUEST
        /////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query_result("$query_select"); // query
        return $this->result; // making result
    }

    public function select_where($column, $table, $identify, $key)
    {
        $query_select_where = "SELECT `" . $column . "` FROM `" . $table . "` WHERE `" . $identify . "` = '" . $key . "';"; // DB REQUEST
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select_where . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select_where"); // query
        return $this->result; // making result
    }

    public function select_where_many($column, $table, $identify, $key, $identify_2, $key_2)
    {
        $query_select_where_many = "SELECT `" . $column . "` FROM `" . $table . "` WHERE `" . $identify . "` = '" . $key . "' AND `" . $identify_2 . "` = '" . $key_2 . "';"; // DB REQUEST
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select_where_many . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select_where_many"); // query
        return $this->result; // making result
    }

    public function select_all_where_many_fixed($table, $identify, $key, $identify_2, $key_2)
    {
        $query_select_all_where_many_fixed = "SELECT * FROM `" . $table . "` WHERE `" . $identify . "` = '" . $key . "' AND `" . $identify_2 . "` = '" . $key_2 . "';"; // DB REQUEST
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select_all_where_many_fixed . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select_all_where_many_fixed"); // query
        return $this->result; // making result
    }

    public function select_all_where($table, $identify, $key)
    {
        $query_select_all_where = "SELECT * FROM `" . $table . "` WHERE `" . $identify . "` = '" . $key . "';"; // DB REQUEST
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select_all_where . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select_all_where"); // query
        return $this->result; // making result
    }

    public function select_all_where_many($table, $identify, $key) /// $key = x,yxz,ada,aaw,..
    {
        $query_select_all_where = "SELECT * FROM `" . $table . "` WHERE `" . $identify . "` IN (" . $key . ");"; // DB REQUEST
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        //echo "Database -->>" . $query_select_all_where . "--<< from Database class <br>"; // query comment for diagnostic
        $this->result = $this->query("$query_select_all_where"); // query
        return $this->result; // making result
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    function __destruct()
    {
        echo 'Object Destroyed:<br>';
        echo '<h6>"Database Class" loading complete... in ' . $this->Timer->get() . ' seconds</h6>';
    }

// SELECT `idproduct` FROM `mainstore` WHERE `idstore` = '1';
// SELECT * FROM `mainstore` WHERE `idstore` = '1';
// SELECT `idmainstore` FROM `mainstore` WHERE `idproduct` = '5';
// SELECT `idmainstore` FROM `mainstore` WHERE `idstore` = '1' AND `idproduct` = '3';
// SELECT `idmainstore`, `idstore`, `idproduct` FROM `mainstore` WHERE `idproduct` = 'x' AND `idstore` = 'y';
// SELECT `idmainstore`, FROM `mainstore` WHERE `idproduct` = 'x' AND `idstore` = 'y';
// SELECT * FROM `person` WHERE `age` IN (12,15,18);
}