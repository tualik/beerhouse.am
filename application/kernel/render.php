<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:15 PM
 * Project: testing.beerhouse.am
 * File: render.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('application')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>render.php</i> Class required ! ....ok </b></div>';
    }
}

class Render
{
    private $registry;
    private $debug;

    function __construct($registry, $debug)
    {
        $this->registry = $registry;
        $this->debug = $debug;
        $this->render($registry, $debug);
    }

    private function render($registry = null, $debug = null)
    {
        require_once SKELETON_DIR . "/dom.php";
    }

    public static function widget($widget, $registry = null, $debug = null)
    {
        require_once HTML_DIR . DIRSEP . $widget . '.tpl';
    }

    public static function show($show, $registry = null, $debug = null)
    {
        require_once HTML_DIR . DIRSEP . $show . '.tpl';
    }

    /**
     * @return mixed
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * @return mixed
     */
    public function getDebug()
    {
        return $this->debug;
    }

} 