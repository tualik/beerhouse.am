<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:16 PM
 * Project: testing.beerhouse.am
 * File: controller.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('application')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>controller.php</i> Class required ! ....ok </b></div>';
    }
}

class Controller
{
    private $registry;
    private $debug;

    function __construct($registry, $debug)
    {
        $this->registry = $registry;
        $this->debug = $debug;
    }

    public function controller_load($controller_name = "main", $method_name = "index", $parameters = null)
    {
        //$controller_name = "Main";
        //var_dump($this->registry->url);
        if (!empty($this->registry->url["type"])) {
            $controller_name = $this->registry->url["type"];
        }
        //$method_name = "index";
        if (!empty($this->registry->url[1])) {
            $method_name = $this->registry->url[1];
        }
        $include = __autoload($controller_name);
        if ($include == false && $controller_name = 'error_404') {
            $this->debug->url = $controller_name = 'error_404';
        }
        /////////////////////////////////////////////////////////////////////////
        //echo $controller_name;
        //echo $method_name;
        if (!empty($this->registry->url[1])) {

            if (method_exists($controller_name, $this->registry->url[1])) {

                $method_name = $this->registry->url[1];
            } else {
                $controller_name = 'error_404';
                $method_name = 'index';
                $this->registry->debug = "method_error";
            }
        }
        ////////////////////////////////////////////////////////////////////////
        $this->registry->controller = new $controller_name($this->registry, $this->debug);
        //echo $method_name;
        $this->registry->controller->$method_name();
        //echo $method_name;
        /////////////////////////////////////////////////////////////////////////

        $this->debug = $this->registry->controller->getDebug();
        $this->registry = $this->registry->controller->getRegistry();
        if ($this->registry->content_template == "") {
            $this->registry->content_template = $controller_name;
        }
        //echo "Back End loaded:..<br>";
        //echo "..:<b>" . $back_name . "</b>:..<br>";

        return true;
    }


    /**
     * @return mixed
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    public function setRegistry($registry)
    {
        $this->registry = $registry;
    }

    /**
     * @return mixed
     */
    public function getDebug()
    {
        return $this->debug;
    }
} 