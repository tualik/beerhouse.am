<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:15 PM
 * Project: testing.beerhouse.am
 * File: router.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('application')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>router.php</i> Class required ! ....ok </b></div>';
    }
}

class Router
{
    private $registry;
    private $debug;

    function __construct($registry, $debug)
    {
        $this->registry = $registry;
        $this->debug = $debug;
        $this->parse_url();
    }

    public static function get_url()
    {
        $url = /*$this->debug->clear(*/
            $_SERVER['REQUEST_URI'] /*)*/
        ;
        if (preg_match('/([^a-zA-Z0-9\.\/\-\_\#])/', $url)) {
            ////// url syntax error
            Router::Error_Page_404();
        }
        $url_array = preg_split('/(\/|\..*$%)/', $url, -1, PREG_SPLIT_NO_EMPTY);
        if (!$url_array) {
            /////////////////////////////////////// root
            unset($url);
            $url[0] = "/";
            $url['type'] = "main";
        } else {
            ///////////////////////////////////// if not root
            unset($url);
            //$url = array();
            $url = $url_array;
            //echo $this->url["url"];
            //var_dump($url_array);
        }

        if (count($url) > Config::$CONFIG["URL_SEGMENT_COUNT"]) {
            //////segments count
            Router::Error_Page_404();
        }
        //some code

        return $url;
    }

    private static function check_url()
    {
        $url = self::get_url();
        $data["column"] = "idurl`, `type";
        $data["identify"] = "url";
        $data["key"] = DataFilter::mysql_clear($url[0]); ///// url first segment
        $data["table"] = "url";
        $request[0] = "connect";
        $request[1] = "select_where";
        $Database = new Db($data, $request);
        $url_data = $Database->getResult();
        if (!empty($url_data)) {
            $url["idurl"] = $url_data[0]["idurl"];
            $url["type"] = $url_data[0]["type"];
        } else {
            ////////// url not exist
            self::Error_Page_404();
        }
        return $url;
    }

    private function parse_url()
    {
        $this->registry->url = self::check_url();
        if (DEBUGGING_MODE == "1") {
            var_dump($this->registry->url);
        }
    }

    public static function Error_Page_404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . 'error_404');
        die;
    }

    /**
     * @return mixed
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * @return mixed
     */
    public function getDebug()
    {
        return $this->debug;
    }
} 