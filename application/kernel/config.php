<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/9/14
 * Time: 12:52 AM
 * Project: testing.beerhouse.am
 * File: config.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('application')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>config.php</i> Class required ! ....ok </b></div>';
    }
}

class Config
{
    const DEBUGGING_MODE = false;
    public static $CONFIG = array(
        'DB_HOST' => 'localhost',
        //'DB_HOST' => '212.34.246.50',
        'DB_USER' => 'admin_hmvc',
        'DB_PASS' => '195927195927ArAm',
        //'DB_PASS' => '',
        'DB_NAME' => 'admin_hmvc',
        'HOST' => 'http://beerhouse.am/',
        'HOST_LOCAL' => 'http://hmvc.local/',
        'URL_SEGMENT_COUNT' => '3',

    );
} 