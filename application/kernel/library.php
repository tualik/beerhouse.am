<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 11:17 PM
 * Project: testing.beerhouse.am
 * File: library.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('application')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>library.php</i> Class required ! ....ok </b></div>';
    }
}

class Library
{
    private $registry;
    private $debug;

    function __construct($registry, $debug)
    {
        $this->registry = $registry;
        $this->debug = $debug;
    }


    /**
     * @return mixed
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * @return mixed
     */
    public function getDebug()
    {
        return $this->debug;
    }
} 