<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 10:11 PM
 * File: categorylist.php
 * Project: public
 */
class CategoryList extends Module
{

    public static function get_category_list($parent)
    {
        $data["table"] = "category";
        $data["identify"] = "idparent";
        $data["key"] = $parent;
        $request[0] = "connect";
        $request[1] = "select_all_where";
        $Database = new Db($data, $request);
        $cat_list_data = $Database->getResult();
        //var_dump($registry);
        //var_dump($registry->cat_list_data);
        unset($Database);
        //// <li><a href=""></a></li>
        return $cat_list_data;
    }
} 