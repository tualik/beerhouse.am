<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 3/1/14
 * Time: 5:36 PM
 * File: opinionwidget.php
 *  * Project: public
 */
class OpinionWidget extends Module
{
    public static function add_opinion()
    {

        if (!empty($_POST["name"])) {
            if (DataFilter::strlen_test($_POST["name"], 3, 32)) {
                $name = DataFilter::clean($_POST["name"]);
                $debug = "ok";
            } else {
                $debug = "Error in length";
                $debug = "ok";

            }
        } else {
            $debug = "Error, please input data";
        }
        if (!empty($_POST["email"])) {
            if (DataFilter::strlen_test($_POST["email"], 10, 32)) {
                $email = DataFilter::clean($_POST["email"]);
                $debug = "ok";
            } else {
                $debug = "Error in length";
            }
        } else {
            $debug = "Error, please input data";
        }
        if (!empty($_POST["message"])) {
            if (DataFilter::strlen_test($_POST["message"], 10, 164)) {
                $message = DataFilter::clean($_POST["message"]);
                $debug = "ok";
            } else {
                $debug = "Error in length";
            }
        } else {
            $debug = "Error, please input data";
        }
        if (!empty($_POST["send_feedback"])) {
            if ($_POST["send_feedback"] == "sended" && $debug == "ok") {
                $submit = DataFilter::clean($_POST["message"]);
                if($debug == "ok" && $name !== "" && $email != "" && $message != ""){
                $debug = self::insert_feedback_message($name, $email, $message);
                } else {
                    $debug = "Error, please input data";
                }
            } else {
                echo $debug;
            }
        } else {
            $debug = "Error, please pick send";
        }
        return $debug;
    }

    private static function insert_feedback_message($name, $email, $message)
    {
        $request[0] = "connect";
        $request[1] = "select_where";
        $data["table"] = "feedback";
        $data["column"] = "username";
        $data["identify"] = "message";
        $data["key"] = $message;
        $q = new Db($data, $request);
        $r = $q->getResult();
        //var_dump($r);
        if ($r == null) {
            unset($data);
            unset($request);
            $server = serialize($_SERVER);
            $request[0] = "connect";
            $request[1] = "insert";
            $data["table"] = "feedback";
            $data[6] = 15;
            $data[7] = ""; //id
            $data[8] = $name; // username
            $today = date("Y-m-d H:i:s");
            $data[9] = $email; // email
            $data[10] = "no data"; // site
            $data[11] = "no data"; // city
            $data[12] = $message; // message
            $data[13] = $today; // date
            $data[14] = $server; // _server_serialized
            $Database = new Db($data, $request);
            $Database->getResult();
            unset($Database);
        } else {
            $debug = "Error in message";
        }
        return $debug;
    }

    public static function get_opinion_widget()
    {
        $data["table"] = "feedback";
        $request[0] = "connect";
        $request[1] = "select";
        $Database = new Db($data, $request);
        $opinion_widget = $Database->getResult();
        //var_dump($opinion_widget);
        unset($Database);
        //// <li><a href=""></a></li>
        return $opinion_widget;
    }
} 