<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 3/5/14
 * Time: 5:52 PM
 * File: dateshow.php
 *  * Project: public
 */
class DateShow extends Module
{

    public static function day($class)
    {
        $select_start = "<select name='day' class='$class'>";
        $option_start = "<option value='";
        $option_middle = "'>";
        $option_end = "</option>";
        $select_end = "</select>";

        $day = $select_start;
        $day .= $option_start . "select_day" . $option_middle . "select day" . $option_end;

        for ($dd = 1; $dd < 32; $dd++) {
            if (strlen($dd) == 1) {
                $dd = "0" . $dd;
            }
            $day .= $option_start . $dd . $option_middle . $dd . $option_end;
        }

        $day .= $select_end;

        return $day;
    }

    public static function month($class)
    {
        $select_start = "<select name='month' class='$class'>";
        $option_start = "<option value='";
        $option_middle = "'>";
        $option_end = "</option>";
        $select_end = "</select>";

        $month = $select_start;
        $month .= $option_start . "select_month" . $option_middle . "select month" . $option_end;
        for ($mm = 1; $mm < 13; $mm++) {
            if (strlen($mm) == 1) {
                $mm = "0" . $mm;
            }
            $month .= $option_start . $mm . $option_middle . $mm . $option_end;
        }
        $month .= $select_end;
        return $month;
    }

    public static function year($class)
    {
        $select_start = "<select name='year' class='$class'>";
        $option_start = "<option value='";
        $option_middle = "'>";
        $option_end = "</option>";
        $select_end = "</select>";

        $year = $select_start;
        $year .= $option_start . "select_year" . $option_middle . "select year" . $option_end;
        for ($yr = 2014; $yr < 2020; $yr++) {
            $year .= $option_start . $yr . $option_middle . $yr . $option_end;
        }
        $year .= $select_end;
        return $year;
    }

    public static function hour($class)
    {
        $select_start = "<select name='hour' class='$class'>";
        $option_start = "<option value='";
        $option_middle = "'>";
        $option_end = "</option>";
        $select_end = "</select>";

        $hour = $select_start;
        $hour .= $option_start . "select_hour" . $option_middle . "select hour" . $option_end;
        for ($hr = 0; $hr < 24; $hr++) {
            if (strlen($hr) == 1) {
                $hr = "0" . $hr;
            }
            $hour .= $option_start . $hr . $option_middle . $hr . $option_end;
        }
        $hour .= $select_end;
        return $hour;
    }

    public static function minute($class)
    {
        $select_start = "<select name='minute' class='$class'>";
        $option_start = "<option value='";
        $option_middle = "'>";
        $option_end = "</option>";
        $select_end = "</select>";

        $minute = $select_start;
        $minute .= $option_start . "select_minute" . $option_middle . "select minute" . $option_end;
        for ($min = 0; $min < 60; $min++) {
            if (strlen($min) == 1) {
                $min = "0" . $min;
            }
            $minute .= $option_start . $min . $option_middle . $min . $option_end;
        }
        $minute .= $select_end;
        return $minute;
    }

    public static function show_date($class)
    {
        $day = self::day($class);
        $month = self::month($class);
        $year = self::year($class);
        $hour = self::hour($class);
        $minute = self::minute($class);

        echo ' Ամսաթիվ ' . $day . '/' . $month . '/' . $year . '  Ժամ  ' . $hour . ' : ' . $minute . ' ';
    }
} 