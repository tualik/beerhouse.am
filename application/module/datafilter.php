<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 9:26 PM
 * File: datafilter.php
 *  * Project: public
 */
class DataFilter extends Module
{
    public static function clean($data)
    {
        if (!empty($data)) {
            if (DEBUGGING_MODE == "1") {
                echo "<br> Clear<br>";
            }
            $data = strip_tags($data);
            $data = trim($data);
            $data = htmlspecialchars($data, ENT_QUOTES);
            $data = preg_replace('/&(\#[0-9]+;)/', '&$1', $data);
            if (get_magic_quotes_gpc()) {
                $data = stripslashes($data);
            }
            if (DEBUGGING_MODE == "1") {
                echo "<br>Clear function filtered data-> " . $data . " <-<br>";
            }
            return $data;
        } else {
            return false;
        }

    }

    public static function mysql_clear($data)
    {
        if (!empty($data)) {
            $data = strip_tags($data);
            $data = trim($data);
            $data = htmlspecialchars($data, ENT_QUOTES);
            $data = preg_replace('/&(\#[0-9]+;)/', '&$1', $data);
            if (get_magic_quotes_gpc()) {
                $data = stripslashes($data);
            }
            if (DEBUGGING_MODE == "1") {
                echo "<br>MySQL Clear function filtered data-> " . $data . " <-<br>";
            }
            //$data = mysql_real_escape_string($data);
            return $data;
        } else {
            return false;
        }
    }

    public static function strlen_test($data, $min, $max, $error = null)
    {

        if (!empty($data)) {

            if (((strlen($data) > $min) || (strlen($data) == $min)) && ((strlen($data) < $max) || (strlen($data) == $max))) {
                if (DEBUGGING_MODE == "1") {
                    $data = html_entity_decode($data);
                    echo $data;
                }
                return true;
            } else {
                if ($error != null) {
                    $alert = "invalid data input !!!";
                    require_once HTML_DIR . '/events/alert.tpl';
                }
                return false;
            }
        } else {
            return false;
        }
    }

} 