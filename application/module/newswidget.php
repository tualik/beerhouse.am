<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 3/1/14
 * Time: 5:36 PM
 * File: newswidget.php
 *  * Project: public
 */
class NewsWidget extends Module
{

    public static function get_news_widget()
    {
        $data["table"] = "news";
        $request[0] = "connect";
        $request[1] = "select";
        $Database = new Db($data, $request);
        $news_widget = $Database->getResult();
        //var_dump($news_widget);
        unset($Database);
        //// <li><a href=""></a></li>
        return $news_widget;
    }
} 