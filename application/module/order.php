<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 3/5/14
 * Time: 7:08 PM
 * File: order.php
 *  * Project: public
 */
class Order extends Module
{
    public static function checkout($query, $query_items)
    {
        //var_dump($query);
        //var_dump($query_items);
        $currency = $query["currency_code"];
        $name = $query["client_name"];
        $email = $query["email"];
        $phone = $query["phone_number"];
        $address = "City/Street : " . $query["city_street"] . " <br>";
        $address .= "Home/Flat Number : " . $query["flat_number"] . " <br>";
        $address .= "Building Number : " . $query["home_number"] . " <br>";
        $address .= "More Details : " . $query["more_details"] . ", ";
        $message = "More Notes : " . $query["notes"] . ", ";
        $delivery_date = $query["date"];
        $user_agent = serialize($_SERVER);
        $user_session = serialize($_SESSION);

        $date = date("Y-m-d H:i:s");
        ///////////////////////////////////////////////////
        if (isset($query_items[1])) {
            $status = 'Pending';
            $count = count($query_items);
            for ($i = 1; $i < $count + 1; $i++) {
                $item_nmb[] = $query_items[$i]["item_number"];
                $item_nme[] = $query_items[$i]["item_name"];
                $item_amt[] = $query_items[$i]["amount"];
                $item_qtt[] = $query_items[$i]["quantity"];
                $tt_price[] = $query_items[$i]["quantity"] * $query_items[$i]["amount"];
            }
            $item_number = implode(",", $item_nmb);
            $item_name = implode(",", $item_nme);
            $amount = implode(",", $item_amt);
            $quantity = implode(",", $item_qtt);
            $count_sum = array_sum($item_qtt);
            $price_sum = array_sum($tt_price);
        } else {
            $status = 'NO ITEMS';
            $item_number = "no";
            $item_name = "no";
            $amount = "no";
            $quantity = "no";
            $count_sum = "no";
            $price_sum = "no";
        }
        //`idorder`, `item_number`, `item_name`, `quantity`, `amount`, `date`, `delivery_date`,
        //`name`, `email`, `address`, `phone`, `message`, `count_sum`, `price_sum`, `user_agent`, `user_session`, `status`
        $request[0] = "connect";
        $request[1] = "insert";
        $data["table"] = "orders";
        $data[6] = 24;
        $data[7] = ""; //idorder
        $data[8] = $item_number;
        $data[9] = $item_name;
        $data[10] = $quantity;
        $data[11] = $amount;
        $data[12] = $date;
        $data[13] = $delivery_date;
        $data[14] = $name;
        $data[15] = $email;
        $data[16] = $address;
        $data[17] = $phone;
        $data[18] = $message;
        $data[19] = $count_sum;
        $data[20] = $price_sum;
        $data[21] = $user_agent;
        $data[22] = $user_session;
        $data[23] = $status;
        $Database = new Db($data, $request);
        $result = $Database->getResult();
        unset($Database);
        //var_dump($result);
        //$alert = "Ձեր Պատվերը Ուղարկված Է!!";
        //include_once HTML_DIR . '/events/alert.tpl';
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('Location:' . $host . '');

    }
} 
