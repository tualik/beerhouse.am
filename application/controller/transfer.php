<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 3/3/14
 * Time: 8:04 PM
 * File: transfer.php
 *  * Project: public
 */
class Transfer extends Controller
{

    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_database($registry, $debug);

    }

    private function get_database($registry, $debug)
    {
        Config::$CONFIG["DB_NAME"] = "beer_house";
        $data["table"] = "categories";
        $request[0] = "connect";
        $request[1] = "select";
        $Database = new Db($data, $request);
        $registry->category_data = $Database->getResult();
        //var_dump($registry);
        //var_dump($registry->article_data);
        unset($Database);
        Config::$CONFIG["DB_NAME"] = "hmvc";
        $this->sort_data($registry, $debug);

    }

    private function sort_data($registry, $debug)
    {
        $count = count($registry->category_data);
        for ($i = 0; $i < $count; $i++) {
            $category_hmvc[$i]["title"] = $registry->category_data[$i]["title_category"];
            $category_hmvc[$i]["description"] = $registry->category_data[$i]["description_category"];
            $category_hmvc[$i]["keywords"] = $registry->category_data[$i]["keyword_category"];
            $category_hmvc[$i]["idparent"] = $registry->category_data[$i]["parent_category"];
            $request[0] = "connect";
            $request[1] = "insert";
            $data["table"] = "category";
            $data[6] = 13;
            $data[7] = "";
            $data[8] = 118 + $i;
            $data[9] = $category_hmvc[$i]["idparent"];
            $data[10] = $category_hmvc[$i]["title"];
            $data[11] = $category_hmvc[$i]["description"];
            $data[12] = $category_hmvc[$i]["keywords"];
            //$Database = new Database($data, $request);
            //$result = $Database->getResult();
            unset($Database);
        }
        //var_dump($category_hmvc);
        /// insert into URL base -> idurl - > from 11 url -> article transliterated title type ->article
        for ($u = 0; $u < count($category_hmvc); $u++) {
            $url[$u]["idurl"] = 118 + $u;
            $url[$u]["url"] = str_replace(" ", "_", $this->arm2translit($category_hmvc[$u]["title"]));
            $url[$u]["type"] = "category";
            $request[0] = "connect";
            $request[1] = "insert";
            $data["table"] = "url";
            $data[6] = 10;
            $data[7] = $url[$u]["idurl"];
            $data[8] = $url[$u]["url"];
            $data[9] = $url[$u]["type"];
            //$Database = new Database($data, $request);
            //$result = $Database->getResult();
            unset($Database);
        }

        //var_dump($url);


    }

    private function send_to_database()
    {

    }

    public static function arm2translit($string)
    {
        $converter = array(
            'ա' => 'a', 'բ' => 'b', 'գ' => 'g',
            'դ' => 'd', 'ե' => 'e', 'զ' => 'z',
            'է' => 'e', 'ը' => 'y', 'թ' => 't',
            'ժ' => 'zh', 'ի' => 'i', 'լ' => 'l',
            'խ' => 'x', 'ծ' => 'ts', 'կ' => 'k',
            'հ' => 'h', 'ձ' => 'dz', 'ղ' => 'x',
            'ճ' => 'ch', 'մ' => 'm', 'յ' => 'j',
            'ն' => 'n', 'շ' => 'sh', 'ո' => 'o',
            'չ' => 'ch', 'պ' => 'p', 'ջ' => 'j',
            'ռ' => "r", 'ս' => 's', 'վ' => "v",
            'տ' => 't', 'ր' => 'r', 'ց' => 'c',
            'ու' => 'u', 'փ' => 'p', 'ք' => 'q',
            'և' => 'ev', 'օ' => 'o', 'ֆ' => 'f',
            'ւ' => 'u',

            'Ա' => 'A', 'Բ' => 'B', 'Գ' => 'G',
            'Դ' => 'D', 'Ե' => 'E', 'Զ' => 'Z',
            'Է' => 'E', 'Ը' => 'Y', 'Թ' => 'T',
            'Ժ' => 'Zh', 'Ի' => 'I', 'Լ' => 'L',
            'Խ' => 'X', 'Ծ' => 'Ts', 'Կ' => 'K',
            'Հ' => 'H', 'Ձ' => 'Dz', 'Ղ' => 'X',
            'Ճ' => 'Ch', 'Մ' => 'M', 'Յ' => 'J',
            'Ն' => 'N', 'Շ' => 'Sh', 'Ո' => 'O',
            'Չ' => 'Ch', 'Պ' => 'P', 'Ջ' => 'J',
            'Ռ' => "R", 'Ս' => 'S', 'Վ' => "V",
            'Տ' => 'T', 'Ր' => 'R', 'Ց' => 'C',
            'Ու' => 'U', 'Փ' => 'P', 'Ք' => 'Q',
            'ԵՎ' => 'Ev', 'Օ' => 'O', 'Ֆ' => 'F',
            'ՈՒ' => 'U', 'Ւ' => 'U', 'ԵՒ' => 'EV',
        );
        $return = strtr($string, $converter);
        //echo $return;
        return $return;
    }
} 