<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 7:44 PM
 * File: article.php
 *  * Project: public
 */
class Article extends Controller
{

    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_article_data($registry, $debug);

    }

    private function get_article_data($registry, $debug)
    {

        $data["column"] = "idarticle`,`idurl`,`idcategory`,`title`,`description`,`keywords`,`text`,`price`,`full_image_url`,`image_alt";
        $data["identify"] = "idurl";
        $data["key"] = DataFilter::mysql_clear($registry->url["idurl"]); ///// url first segment
        $data["table"] = "article";
        $request[0] = "connect";
        $request[1] = "select_where";
        $Database = new Db($data, $request);
        $registry->article_data = $Database->getResult();
        $registry->dom_title = $_SESSION["category"] . " | -> " . $registry->article_data[0]["title"];
        //var_dump($registry);
        //var_dump($registry->article_data);
        unset($Database);
    }

} 