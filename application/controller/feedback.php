<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 7:46 PM
 * File: feedback.php
 *  * Project: public
 */
class Feedback extends Controller
{
    function Add()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        $registry->dom_title = "Կարծիք Թողնել";
        $registry->content_template = "feedback_add";
        //$registry = $this->getRegistry();
        $submit = DataFilter::mysql_clear($_POST["send_feedback"]);
        if (isset($submit) && $submit == "sended") {
            $debug = OpinionWidget::add_opinion();
            if ($debug == "ok") {
                $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
                header('Location:' . $host . 'feedback');
            } else {
                $alert = $debug;
                require_once HTML_DIR . "/events/alert.tpl";
            }
        }
    }

    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        $registry->dom_title = "Կարծիքներ";

        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_feedback_data($registry, $debug);
        $this->setRegistry($registry);
    }

    private function get_feedback_data($registry, $debug)
    {
        $data["table"] = "feedback";
        $request[0] = "connect";
        $request[1] = "select";
        $Database = new Db($data, $request);
        $registry->feedback_data = $Database->getResult();
        unset($Database);
    }

    function Opinion()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        $this->get_feedback_message_data($registry, $debug);
        $registry->content_template = "feedback_article";
        $this->setRegistry($registry);
    }

    private function get_feedback_message_data($registry, $debug)
    {
        $id = DataFilter::mysql_clear($registry->url[2]);
        //echo $id;
        if (DataFilter::strlen_test($id, 1, 3)) {
            //if(is_int($id)) {
            $data["key"] = $id; ///// url third segment
            $data["column"] = "username`,`message";
            $data["identify"] = "idfeedback";
            $data["table"] = "feedback";
            $request[0] = "connect";
            $request[1] = "select_where";
            $Database = new Db($data, $request);
            $registry->feedback_article_data = $Database->getResult();
            $registry->dom_title = $registry->feedback_article_data[0]["title"];
            unset($Database);
            //}
        }
    }
}