<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 7:46 PM
 * File: news.php
 *  * Project: public
 */
class News extends Controller
{

    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_news_data($registry, $debug);
        $registry->dom_title = "Նորություններ";
        $this->setRegistry($registry);
    }

    private function get_news_data($registry, $debug)
    {
        // Полные тексты 	idnews 	idurl 	title 	description 	text 	date
        $data["table"] = "news";
        $request[0] = "connect";
        $request[1] = "select";
        $Database = new Db($data, $request);
        $registry->news_data = $Database->getResult();
        //var_dump($registry);
        //var_dump($registry->news_data);
        unset($Database);
    }

    function Article()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        $this->get_news_article_data($registry, $debug);
        $registry->content_template = "news_article";

        $this->setRegistry($registry);
    }

    private function get_news_article_data($registry, $debug)
    {
        // Полные тексты 	idnews 	idurl 	title 	description 	text 	date
        // if(is_int($registry->url[2])){ECHO "fuck";}
        $id = DataFilter::mysql_clear($registry->url[2]);
        //echo $id;
        if (DataFilter::strlen_test($id, 1, 3)) {
            //if(is_int($id)) {

            $data["key"] = $id; ///// url third segment
            $data["column"] = "title`,`text`,`date";
            $data["identify"] = "idnews";
            $data["table"] = "news";
            $request[0] = "connect";
            $request[1] = "select_where";
            $Database = new Db($data, $request);
            $registry->news_article_data = $Database->getResult();
            $registry->dom_title = "Նորություններ | " . $registry->news_article_data [0]["title"];
            //var_dump($registry);
            //var_dump($registry->news_article_data);
            unset($Database);
            //}
        }


    }


}