<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 7:45 PM
 * File: page.php
 *  * Project: public
 */
class Page extends Controller
{
    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_page_data($registry, $debug);

    }

    private function get_page_data($registry, $debug)
    {
        $data["column"] = "idpage`,`idurl`,`title`,`description`,`keywords`,`text";
        $data["identify"] = "idurl";
        $data["key"] = DataFilter::mysql_clear($registry->url["idurl"]); ///// url first segment
        $data["table"] = "page";
        $request[0] = "connect";
        $request[1] = "select_where";
        $Database = new Db($data, $request);
        $registry->page_data = $Database->getResult();
        $registry->dom_title = $registry->page_data[0]["title"];
        if ($registry->page_data[0]["title"] == "Կապ") {
            $registry->attaching = "map_head";
        }
        //var_dump($registry);
        //var_dump($registry->page_data);
        unset($Database);
    }
} 