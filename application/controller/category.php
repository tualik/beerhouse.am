<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 7:44 PM
 * File: category.php
 *  * Project: public
 */
class Category extends Controller
{
    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_category_data($registry, $debug);
        $this->setRegistry($registry);
    }

    private function get_category_data($registry, $debug)
    {
        $data["column"] = "idcategory`,`idurl`,`idparent`,`title`,`description`,`keywords";
        $data["identify"] = "idurl";
        $data["key"] = DataFilter::mysql_clear($registry->url["idurl"]); ///// url first segment
        $data["table"] = "category";
        $request[0] = "connect";
        $request[1] = "select_where";
        $Database = new Db($data, $request);
        $registry->category_data = $Database->getResult();
        $registry->dom_title = $registry->category_data[0]["title"];
        $_SESSION["category"] = $registry->category_data[0]["title"];
        //var_dump($registry);
        //var_dump($registry->category_data);
        unset($Database);
        //$data["column"] = "idcategory`,`idurl`,`idparent`,`title`,`description`,`keywords";
        $data["identify"] = "idcategory";
        $data["key"] = $registry->category_data[0]["idcategory"]; ///// url first segment
        $data["table"] = "article";
        $request[0] = "connect";
        $request[1] = "select_all_where";
        $Database = new Db($data, $request);
        $registry->articles_data = $Database->getResult();
        //var_dump($registry);
        //var_dump($registry->articles_data);
        unset($Database);
    }

    public static function get_all_url()
    {
        $data["table"] = "url";
        $request[0] = "connect";
        $request[1] = "select";
        $Database = new Db($data, $request);
        $url_array = $Database->getResult();
        unset($Database);
        return $url_array;
    }

    public static function get_url($id_url, $url_array)
    {
        $count = count($url_array);
        for ($i = 0; $i < $count; $i++) {
            if ($url_array[$i]["idurl"] == $id_url) {
                $url = $url_array[$i]["url"];
            }
        }
        echo $url;
    }
} 