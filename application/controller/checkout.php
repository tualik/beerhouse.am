<?php

/**
 * Created by Aram Harutyunyan.
 * Owner: quad9550
 * Date: 2/28/14
 * Time: 7:45 PM
 * File: checkout
 *  * Project: public
 */
class Checkout extends Controller
{
    function Index()
    {
        $registry = $this->getRegistry();
        $debug = $this->getDebug();
        $registry->dom_title = "Պատվիրել";
        if (!empty($registry->url[1])) {
            $registry->url[1] = "";
        }
        $this->get_checkout_page_data($registry, $debug);
    }

    private function get_checkout_page_data($registry, $debug)
    {
        $data["column"] = "idpage`,`idurl`,`title`,`description`,`keywords`,`text";
        $data["identify"] = "idurl";
        $data["key"] = DataFilter::mysql_clear($registry->url["idurl"]); ///// url first segment
        $data["table"] = "page";
        $request[0] = "connect";
        $request[1] = "select_where";
        $Database = new Db($data, $request);
        $registry->checkout_page_data = $Database->getResult();
        //var_dump($registry);
        //var_dump($registry->checkout_page_data);
        unset($Database);
    }

    function gate()
    {
        $this->Index();
        include(ROOT_DIR . "/jcart/gateway.php");
    }
} 