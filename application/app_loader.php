<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 10:28 PM
 * Project: testing.beerhouse.am
 * File: app_loader.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('application')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>app_loader.php</i> Class required ! ....ok </b></div>';
    }
}

class App_Loader
{
    public static $registry = array();
    public static $debug = array();

    public static function run_application($data)
    {
        $Kernel = new App_Loader();
        $Kernel->load_kernel($data);
        self::$registry->kernel = $Kernel;
    }

    private function load_kernel($data)
    {
        $this->load_registry();
        $this->load_database();
        $this->load_library();
        $this->load_router();
        $this->load_module();
        $this->load_cart($data);
        $this->load_controller();
        $this->debugger();
        $this->load_render();
    }

    private function load_registry()
    {
        self::$registry = new Registry();
    }

    private function load_router()
    {
        self::$registry->router = new Router(self::$registry, self::$debug);
        self::$registry = self::$registry->router->getRegistry();
        self::$debug = self::$registry->router->getDebug();
    }

    private function load_controller()
    {
        self::$registry->controller = new Controller(self::$registry, self::$debug);
        self::$registry->controller->controller_load(self::$registry->url["type"]);
        self::$registry = self::$registry->controller->getRegistry();
        self::$debug = self::$registry->controller->getDebug();
    }

    private function load_database()
    {
        //self::$registry->database = new Database(self::$registry, self::$debug);
        //self::$registry = self::$registry->database->getRegistry();
        //self::$debug = self::$registry->database->getDebug();
    }

    private function load_library()
    {
        self::$registry->library = new Library(self::$registry, self::$debug);
        self::$registry = self::$registry->library->getRegistry();
        self::$debug = self::$registry->library->getDebug();
    }

    private function load_module()
    {
        self::$registry->module = new Module(self::$registry, self::$debug);
        self::$registry->module->startup();
        self::$registry = self::$registry->module->getRegistry();
        self::$debug = self::$registry->module->getDebug();
    }

    private function debugger()
    {
        if (DEBUGGING_MODE == "1") {
            // var_dump(self::$registry);
            // var_dump(self::$debug);
        }

    }

    private function load_cart($data)
    {
        self::$registry->cart = $data;
    }

    private function load_render()
    {
        self::$registry->render = new Render(self::$registry, self::$debug);
        self::$registry = self::$registry->render->getRegistry();
        self::$debug = self::$registry->render->getDebug();
    }


}