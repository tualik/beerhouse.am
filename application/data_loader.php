<?php
/**
 * Created by Aram Harutyunyan.
 * Owner: Aram Harutyunyan
 * Date: 2/8/14
 * Time: 7:36 PM
 * Project: testing.beerhouse.am
 * File: data_loader.php
 * All Rights Reserved
 * Copyright © 2013
 * Email: admin@3M-LiFe.com
 * Web: http://www.3M-LiFe.com
 */
if (!defined('data_loader')) {
    die ("Hacking attempt!");
} else {
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>data_loader.php</i> required ! ....ok </b></div>';
    } else {
    }
}
function SetConstants()
{
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>SetConstants()</i> Loaded ....; </b></div>';
    } else {
    }
    define ('KERNEL_DIR', ROOT_DIR . '/application/kernel');
    define ('LANG_DIR', ROOT_DIR . '/application/lang');
    define ('HTML_DIR', ROOT_DIR . '/html');
    define ('SKELETON_DIR', ROOT_DIR . '/html/skeleton');
    define ('UPLOAD_DIR', ROOT_DIR . '/upload');
    define ('MODULE_DIR', ROOT_DIR . '/application/module');
    define ('CONTROLLER_DIR', ROOT_DIR . '/application/controller');
    define ('LIBRARY_DIR', ROOT_DIR . '/application/library');
    define ('DB_DIR', ROOT_DIR . '/application/database');
    define ('CART_DIR', ROOT_DIR . '/application/jcart');
}

SetConstants(); /// Defining Path in constants

function StartDiagnostics()
{
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>StartDiagnostics()</i> Loaded ....; </b></div>';
    }
    if (!defined('E_DEPRECATED')) {
        error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
        ini_set('error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE);
    } else {
        error_reporting(E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE);
        ini_set('error_reporting', E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE);
    }
    ini_set('display_errors', true);
    ini_set('html_errors', true);
}

function CachePerformance()
{
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>CachePerformance()</i> Loaded ....; </b></div>';
    }
    ob_start();
    ob_implicit_flush(1);
}

function __autoload($class_name)
{
    $filename = strtolower($class_name) . '.php';
    //echo $filename;
    $file[0] = ROOT_DIR . DIRSEP . $filename;
    $file[1] = KERNEL_DIR . DIRSEP . $filename;
    $file[2] = APPLICATION_DIR . DIRSEP . $filename;
    $file[3] = LANG_DIR . DIRSEP . $filename;
    $file[4] = MODULE_DIR . DIRSEP . $filename;
    $file[5] = CONTROLLER_DIR . DIRSEP . $filename;
    $file[6] = LIBRARY_DIR . DIRSEP . $filename;
    $file[7] = LANG_DIR . DIRSEP . $filename;
    $file[8] = DB_DIR . DIRSEP . $filename;
    $file[9] = CART_DIR . DIRSEP . $filename;
    if (DEBUGGING_MODE == "1") {
        //var_dump($file);
    }
    $count = count($file);
    for ($i = 0; $i < $count; $i++) {
        if (file_exists($file[$i]) == true) {
            $real_file = $file[$i];
        }
    }
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>__autoload()</i> Loading File ' . $real_file . '.... </b></div>';
    }
    if (empty($real_file)) {
        unset($real_file);
        return false;
    }
    if (!empty($real_file)) {
        if (!empty($real_file)) {
            require_once $real_file;
        }
        return true;
    }
    return $real_file;
}

function SetSecurity()
{
    define ('application', true);
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>SetSecurity()</i> Loaded ....; </b></div>';
    }
}

function jcart_loader()
{

}

function LoadScriptLoader($data)
{
    global $App_Loader;
    spl_autoload_register('__autoload');
    if (DEBUGGING_MODE == "1") {
        echo '<div><b><i>LoadScriptLoader()</i> Loaded ....; </b></div>';
    }
    $App_Loader = new App_Loader();
    App_Loader::run_application($data);
}

function LoadDiagnostics()
{
    if (DEBUGGING_MODE == "1") {
        echo '<br><div><b><i>LoadDiagnostics()</i> Loaded ....; </b></div>';
        echo '<hr><div style="font-size:16px; font-style:bold;">Generation Timer->  ' . MicroTimer::get() . ' </div><hr>';
        echo '<div style="font-size: 12px; "><table border="2">';
        echo '<tr><td><b>--> $_SESSION ->>>></b></td><td><div>';
        var_dump($_SESSION);
        echo '</div></td></tr><tr><td><b>--> $_POST ->>>></b></td><td><div>';
        var_dump($_POST);
        echo '</div></td></tr><tr><td><b>--> $_GET ->>>></b></td><td><div>';
        var_dump($_GET);
        echo '</div></td></tr><tr><td><b>--> $_REQUEST ->>>></b></td><td><div>';
        var_dump($_REQUEST);
        echo '</div></td></tr><tr><td><b>--> $registry ->>>></b></td><td><div>';
        var_dump(App_Loader::$registry);
        echo '</div></td></tr><tr><td><b>--> $debug ->>>></b></td><td><div>';
        var_dump(App_Loader::$debug);
        echo '</div></td></tr><tr><td colspan="2"><b>time: ' . MicroTimer::get() . '  :-) </b></td></tr><tr><td colspan="2"><div>';
        //session_destroy();
        echo '</div><b>__/> "session_destroy()"  loaded... </b><br></td></tr><tr><td colspan="2"><b> time: ' . MicroTimer::get() . ' :-P </b></td></tr>';
        echo '<tr><td><b>--> $_SESSION ->>>></b><br><div></td><td>';
        var_dump($_SESSION);
        echo '</div></td></tr><br><tr><td colspan="2"><b><___/> ...<br>';
        echo '</div></td></tr><tr><td><div>';
        //phpinfo(1);
        //echo '</div></td></tr><tr><td><div>';
        //echo '</div><b>__/> "phpinfo()"  loaded... </b><br></td></tr><tr><td colspan="2"><b> time: ' . MicroTimer::get() . ' :-P </b></td></tr>';
        echo '</div></td></tr></table>';
        echo '<hr><div style="font-size:16px; font-style:bold;">Generation Timer->  ' . MicroTimer::get() . ' </div><hr>';
        //var_dump($_SESSION);
    }


}