<?php

//include_once('jcart.php');
$registry = $this->getRegistry();
$jcart = $registry->cart;
//var_dump($_POST);

$config = $jcart->config;

// The update and empty buttons are displayed when javascript is disabled 
// Re-display the cart if the visitor has clicked either button
if ($_POST['jcartUpdateCart'] || $_POST['jcartEmpty']) {

    // Update the cart
    if ($_POST['jcartUpdateCart']) {
        if ($jcart->update_cart() !== true) {
            $_SESSION['quantityError'] = true;
        }
    }

    // Empty the cart
    if ($_POST['jcartEmpty']) {
        $jcart->empty_cart();
    }

    // Redirect back to the checkout page
    $protocol = 'http://';
    if (!empty($_SERVER['HTTPS'])) {
        $protocol = 'https://';
    }

    header('Location: ' . $protocol . $_SERVER['HTTP_HOST'] . $config['checkoutPath']);
    exit;
} // The visitor has clicked the PayPal checkout button
else {

    ////////////////////////////////////////////////////////////////////////////
    /*

    A malicious visitor may try to change item prices before checking out.

    Here you can add PHP code that validates the submitted prices against
    your database or validates against hard-coded prices.

    The cart data has already been sanitized and is available thru the
    $jcart->get_contents() method. For example:

    foreach ($jcart->get_contents() as $item) {
        $itemId	    = $item['id'];
        $itemName	= $item['name'];
        $itemPrice	= $item['price'];
        $itemQty	= $item['qty'];
    }

    */
    ////////////////////////////////////////////////////////////////////////////

    // For now we assume prices are valid
    $validPrices = true;

    ////////////////////////////////////////////////////////////////////////////

    // If the submitted prices are not valid, exit the script with an error message
    if ($validPrices !== true) {
        die($config['text']['checkoutError']);
    }
    // Price validation is complete
    // Send cart contents to PayPal using their upload method, for details see: http://j.mp/h7seqw
    elseif ($validPrices === true) {
        // Paypal count starts at one instead of zero
        $count = 1;
        if (DataFilter::mysql_clear($_POST["jcartPaypalCheckout"]) == "Վճարել PayPal֊ով") {
            //echo "fuck2";
            // Build the query string
            $queryString = "?cmd=_cart";
            $queryString .= "&upload=1";
            $queryString .= "&charset=utf-8";
            $queryString .= "&currency_code=" . urlencode($config['currencyCode']);
            $queryString .= "&business=" . urlencode($config['paypal']['id']);
            $queryString .= "&return=" . urlencode($config['paypal']['returnUrl']);
            $queryString .= '&notify_url=' . urlencode($config['paypal']['notifyUrl']);

            foreach ($jcart->get_contents() as $item) {

                $queryString .= '&item_number_' . $count . '=' . urlencode($item['id']);
                $queryString .= '&item_name_' . $count . '=' . urlencode($item['name']);
                $queryString .= '&amount_' . $count . '=' . urlencode($item['price']);
                $queryString .= '&quantity_' . $count . '=' . urlencode($item['qty']);

                // Increment the counter
                ++$count;
            }
        }
        if (DataFilter::mysql_clear($_POST["jcartCheckoutCache"]) != "") {
            //var_dump($jcart);
            $queryString ["currency_code"] = $config['currencyCode'];
            ////////////////////////////////////////////////////////////////
            //POST DATA
            if (DataFilter::strlen_test($_POST["name"], 4, 64)) {
                $queryString ["client_name"] = DataFilter::mysql_clear($_POST["name"]);
            }
            if (DataFilter::strlen_test($_POST["email"], 10, 32)) {
                $queryString ["email"] = DataFilter::mysql_clear($_POST["email"]);
            }
            if (DataFilter::strlen_test($_POST["phone_number"], 12, 15)) {
                $queryString ["phone_number"] = DataFilter::mysql_clear($_POST["phone_number"]);
                $continue = true;
            } else {
                $continue = false;
                $alert = "Մուտքագրեք Ճիշտ Հեռախոսահամար!!";
                include_once HTML_DIR . '/events/alert.tpl';
            }
            if (DataFilter::strlen_test($_POST["city_street"], 10, 64)) {
                $queryString ["city_street"] = DataFilter::mysql_clear($_POST["city_street"]);
            }
            if (DataFilter::strlen_test($_POST["flat_number"], 1, 5)) {
                $queryString ["flat_number"] = DataFilter::mysql_clear($_POST["flat_number"]);
            }
            if (DataFilter::strlen_test($_POST["home_number"], 1, 5)) {
                $queryString ["home_number"] = DataFilter::mysql_clear($_POST["home_number"]);
            }
            if (DataFilter::strlen_test($_POST["more_details"], 5, 255)) {
                $queryString ["more_details"] = DataFilter::mysql_clear($_POST["more_details"]);
            }
            if (DataFilter::strlen_test($_POST["day"], 1, 2)) {
                $queryString ["date"] = "date " . DataFilter::mysql_clear($_POST["day"]) . "/";
                if ($_POST["day"] == "select_day") {
                    $queryString["date"] = "Deliver now !!!";
                }
            }
            if (DataFilter::strlen_test($_POST["month"], 1, 2)) {
                $queryString ["date"] .= DataFilter::mysql_clear($_POST["month"]) . "/";
                if ($_POST["month"] == "select_month") {
                    $queryString["date"] = "Deliver now !!!";
                }
            }
            if (DataFilter::strlen_test($_POST["year"], 4, 4)) {
                $queryString ["date"] .= DataFilter::mysql_clear($_POST["year"]) . " time ";
                if ($_POST["year"] == "select_year") {
                    $queryString["date"] = "Deliver now !!!";
                }
            }
            if (DataFilter::strlen_test($_POST["hour"], 1, 2)) {
                $queryString ["date"] .= DataFilter::mysql_clear($_POST["hour"]) . ":";
                if ($_POST["hour"] == "select_hour") {
                    $queryString["date"] = "Deliver now !!!";
                }
            }
            if (DataFilter::strlen_test($_POST["minute"], 1, 2)) {
                $queryString ["date"] .= DataFilter::mysql_clear($_POST["minute"]);
                if ($_POST["minute"] == "select_minute") {
                    $queryString["date"] = "Deliver now !!!";
                }
            }
            if (DataFilter::strlen_test($_POST["notes"], 10, 32)) {
                $queryString ["notes"] = DataFilter::mysql_clear($_POST["notes"]);
            }
            if (!isset($queryString["date"])) {
                $queryString["date"] = "Deliver now !!!";
            }

            foreach ($jcart->get_contents() as $item) {

                $query_items[$count]["item_number"] = $item['id'];
                $query_items[$count]["item_name"] = $item['name'];
                $query_items[$count]["amount"] = $item['price'];
                $query_items[$count]["quantity"] = $item['qty'];

                // Increment the counter
                ++$count;
            }
            if ($continue === true) {
                // Send the visitor to PayPal
                $jcart->empty_cart();
                Order::checkout($queryString, $query_items);
                //var_dump($queryString);
            }
        }
        // Empty the cart


        // Confirm that a PayPal id is set in config.php
        if ($config['paypal']['id']) {
            // Add the sandbox subdomain if necessary
            $sandbox = '';
            if ($config['paypal']['sandbox'] === true) {
                $sandbox = '.sandbox';
            }
            // Use HTTPS by default
            $protocol = 'https://';
            if ($config['paypal']['https'] == false) {
                $protocol = 'http://';
            }
            if (DataFilter::mysql_clear($_POST["jcartPaypalCheckout"]) == "Վճարել PayPal֊ով") {
                $jcart->empty_cart();
                // Send the visitor to PayPal
                @header('Location: ' . $protocol . 'www' . $sandbox . '.paypal.com/cgi-bin/webscr' . $queryString);
                //var_dump($queryString);
            }
        } else {
            die('Couldn&rsquo;t find a PayPal ID in <strong>config.php</strong>.');
        }
    }
}