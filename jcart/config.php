<?php

// jCart v1.3
// http://conceptlogic.com/jcart/

// Do NOT store any sensitive info in this file!!!
// It's loaded into the browser as plain text via Ajax


////////////////////////////////////////////////////////////////////////////////
// REQUIRED SETTINGS

// Path to your jcart files
$config['jcartPath']              = 'jcart/';

// Path to your checkout page
$config['checkoutPath']           = '/checkout';

// The HTML name attributes used in your item forms
$config['item']['id']             = 'my-item-id';    // Item id
$config['item']['name']           = 'my-item-name';    // Item name
$config['item']['price']          = 'my-item-price';    // Item price
$config['item']['qty']            = 'my-item-qty';    // Item quantity
$config['item']['url']            = 'my-item-url';    // Item URL (optional)
$config['item']['add']            = 'my-add-button';    // Add to cart button

// Your PayPal secure merchant ID
// Found here: https://www.paypal.com/webapps/customerprofile/summary.view
$config['paypal']['id']           = 'HLQFRXK846S3U';

////////////////////////////////////////////////////////////////////////////////
// OPTIONAL SETTINGS

// Three-letter currency code, defaults to USD if empty
// See available options here: http://j.mp/agNsTx
$config['currencyCode']           = 'AMD';

// Add a unique token to form posts to prevent CSRF exploits
// Learn more: http://conceptlogic.com/jcart/security.php
$config['csrfToken']              = true;

// Override default cart text
$config['text']['cartTitle']      = 'Զամբյուղ';    // Shopping Cart
$config['text']['singleItem']     = 'հատ';    // Item
$config['text']['multipleItems']  = 'հատ';    // Items
$config['text']['subtotal']       = 'Ընդամենը';    // Subtotal
$config['text']['update']         = 'Թարմացնել';    // update
$config['text']['checkout']       = 'Պատվիրել';    // checkout
$config['text']['checkoutPaypal'] = 'Վճարել PayPal֊ով';    // Checkout with PayPal
$config['text']['checkoutCache']  = 'Վճարել Տեղում';    // Checkout with PayPal
$config['text']['removeLink']     = 'Հեռացնել';    // remove
$config['text']['emptyButton']    = 'Դատարկել';    // empty
$config['text']['emptyMessage']   = 'Զամբյուղը Դատարկ Է։';    // Your cart is empty!
$config['text']['itemAdded']      = 'Ուղարկվեց Զամբյուղ';    // Item added!
$config['text']['priceError']     = 'Սխալ Գին։';    // Invalid price format!
$config['text']['quantityError']  = 'Սխալ Քանակ։';    // Item quantities must be whole numbers!
$config['text']['checkoutError']  = 'Զեր Պատոերը ՀՆԱՐԱՎՈՐ ՉԷ! Մշակել։';    // Your order could not be processed!

// Override the default buttons by entering paths to your button images
$config['button']['checkout']     = '';
$config['button']['paypal']       = '';
$config['button']['update']       = '';
$config['button']['empty']        = '';


////////////////////////////////////////////////////////////////////////////////
// ADVANCED SETTINGS

// Display tooltip after the visitor adds an item to their cart?
$config['tooltip']                = true;

// Allow decimals in item quantities?
$config['decimalQtys']            = false;

// How many decimal places are allowed?
$config['decimalPlaces']          = 1;

// Number format for prices, see: http://php.net/manual/en/function.number-format.php
$config['priceFormat']            = array('decimals' => 2, 'dec_point' => '.', 'thousands_sep' => ',');

// Send visitor to PayPal via HTTPS?
$config['paypal']['https']        = true;

// Use PayPal sandbox?
$config['paypal']['sandbox']      = false;

// The URL a visitor is returned to after completing their PayPal transaction
$config['paypal']['returnUrl']    = '/';

// The URL of your PayPal IPN script
$config['paypal']['notifyUrl']    = '';

?>